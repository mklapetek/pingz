/*
 * Copyright (C) 2012 by Martin Klapetek <martin.klapetek@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "pingzapplet.h"

#include <QGraphicsLinearLayout>
#include <QTimer>

#include <KIconLoader>

#include <Plasma/ToolTipManager>
#include <Plasma/Svg>

PingzApplet::PingzApplet(QObject *parent, const QVariantList &args)
    : Plasma::Applet(parent, args)
{
    setAspectRatioMode(Plasma::ConstrainedSquare);
    setMinimumSize(16, 16);
    setBackgroundHints(NoBackground);
    resize(150, 150);

    m_icon = new Plasma::IconWidget(this);
    m_icon->setIcon("user-offline");

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout();
    layout->setContentsMargins(2, 2, 2, 2);
    layout->setSpacing(0);
    layout->setOrientation(Qt::Horizontal);
    layout->addItem(m_icon);
    layout->setAlignment(m_icon, Qt::AlignCenter);
    setLayout(layout);

    if (formFactor() == Plasma::Planar) {
        int iconSize = IconSize(KIconLoader::Small);
        setMinimumSize(QSize(iconSize, iconSize));
    }

    setStatus(Plasma::PassiveStatus);

    // register plasmoid for tooltip
    Plasma::ToolTipManager::self()->registerWidget(this);
}

PingzApplet::~PingzApplet()
{
}

void PingzApplet::init()
{
    kDebug();
    m_process = new QProcess(this);
    connect(m_process, SIGNAL(finished(int,QProcess::ExitStatus)),
            this, SLOT(pingFinished(int,QProcess::ExitStatus)));

    m_timer = new QTimer(this);
    m_timer->setInterval(5000);
    connect(m_timer, SIGNAL(timeout()),
            this, SLOT(ping()));
    m_timer->start();
    m_process->start("ping", QStringList() << "-c1" << "google.com");
}

void PingzApplet::ping()
{
    if (m_process->state() == QProcess::Running) {
        //kill it first if it is still running
        m_process->kill();
        kDebug() << "Killing qprocess";
    }
    m_process->start("ping", QStringList() << "-c1" << "google.com");
}

void PingzApplet::pingFinished(int exitCode, QProcess::ExitStatus status)
{
    kDebug() << exitCode << status;
    if (exitCode == 0 && status == QProcess::NormalExit) {
        m_icon->setIcon("user-online");
    } else {
        m_icon->setIcon("user-offline");
    }
    m_process->kill();
}

void PingzApplet::toolTipAboutToShow()
{
    Plasma::ToolTipContent content;
    if (m_icon->icon().name() == QLatin1String("user-online")) {
        content.setMainText(i18n("Network available"));
    } else {
        content.setMainText(i18n("Network not available"));
    }

    content.setImage(m_icon->icon());

    Plasma::ToolTipManager::self()->setContent(this, content);
}

void PingzApplet::toolTipHidden()
{
    Plasma::ToolTipManager::self()->clearContent(this);
}

#include "pingzapplet.moc"

// This is the command that links your applet to the .desktop file
K_EXPORT_PLASMA_APPLET(pingz, PingzApplet)
